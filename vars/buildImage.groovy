#!/user/bin/env groovy

def call(){
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
    sh 'docker build -t imayur/demo-app:jma-2.0 .'
    sh 'each $PASS | docker login -u $USER --password-stdin'
    sh 'docker push imayur/demo-app:jma-2.0'
    }
}

